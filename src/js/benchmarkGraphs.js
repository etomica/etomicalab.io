var graphs = [];

$.getJSON("http://10.0.1.18:5000/results", function (data) {
    console.log(data)
    $.each(data["benchmarks"], function (name, results) {
        results.data.forEach(function (part, i, arr) {
            arr[i][0] = new Date(part[0]);
        });

        var simName = name.split(".")[2];
        
        var graphDiv = $("<div></div>")
            .attr("id", simName)
            .addClass("benchmark-graph");


        $("#graphs-container").append(graphDiv);
        var g = new Dygraph(
            graphDiv[0],
            results.data,
            {
                title: simName,
                xlabel: "Date",
                ylabel: "Integrator Steps per Second",
                axisLabelWidth: 80,
                labels: results.labels,
                legend: 'follow',
                labelsSeparateLines: true,
                errorBars: true,
                showRangeSelector: true,
                interactionModel: Dygraph.defaultInteractionModel,
                rangeSelectorPlotFillGradientColor: "",
                drawPoints: true,
                axes: {
                    x: { drawGrid: false },
                    y: { drawGrid: false }
                },
                pointClickCallback: function (e, point) {
                    commit = data.commits[point.idx];
                    console.log(commit);
                    window.open("https://github.com/etomica/etomica/commit/" + commit, "_blank");
                }
            }
        );
        graphs.push(g);
    })

    var sync = Dygraph.synchronize(graphs, { range: false });
});